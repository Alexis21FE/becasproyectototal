<?php 
session_start();  

?>		 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Estudios Socioeconomicos | Iniciar sesion </title>
<link rel="stylesheet" href="css/login/screen.css" type="text/css" media="screen" title="default" />
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
<!--  jquery core -->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="../js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<-->
    <script src="../js/sweetalert2/dist/sweetalert2.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

<!-- Custom jquery scripts -->
<script src="../js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="../js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<style>
    label{
        color:black;
    }
</style>
</head>
<body > 
  
    <div style="width:100%;height: 10%;">
       
    </div>
    <div style="margin-top: 4%;">
        <div style="width: 25%;margin:auto;height: 40%;">
            <form name="form_log" action="funciones/login.php" id="form_login" method="post">
            <div class="row">
                <input type="hidden" name="funcion" value="login">
                <div class="col"><label>Usuario</label>
                    <input class="form-control" name="user" required type="text">
                </div>
                </div>
                <div class="row">
                <div class="col">
                    <label>Password</label>
                    <input type="password" name="pass" class="form-control  " required >
                </div>
            </div>
 <div class="row" style="margin-top: 4%;">
                <div class="col">
                    <button type="button" name="acceder" class="btn" style="background-color: #b9a92f;" id="btn_login" >Acceder</button>
                   
                </div>
                  <a href="#"  id="forguet_pass">¿Olvidaste tu contraseña?</a>
            </div>
            </form>
        </div>
    </div>

    </div>
    
<script type="text/javascript">
    $("#forguet_pass").click(function(){
        swal("Favor de comunicarte al Colegio");
    });
$("#btn_login").click(function(event){

var form =$("#form_login").serialize();
$.ajax({
    url:"Funciones/login.php",
    data:form,
    type:"post",
    datatype:"json",
    success:function(response){

            if(response.status!=true){
            swal(response.mensaje);
        }else{
            var url="estudios.php";
             swal.fire(response.mensaje,'','success');
      setTimeout(function(){
           window.location.replace(url);
         }, 2000);
      }

    
        },error:function(){
        }
        });
});</script>
</body>
</html>
