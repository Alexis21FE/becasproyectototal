<?php 
session_start();
$_SESSION["var_ban"]=3;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$band_error_msg=(isset ($_GET["red_msg"]) ? $_GET["red_msg"]: "");
$band_modf_msg=(isset ($_GET["modf_msg"]) ? $_GET["modf_msg"]: "");	
extract($_POST, EXTR_PREFIX_ALL, "x");
include("z_script/db_class.php");
mysql_select_db($bd_becas,$link);

//paginador
if(isset($_GET['page'])) //verifica pagina
{
	$page= $_GET['page'];
}else{
	$page=1;
}

if(isset($x_busc)){
		
		if($x_tip_bus == NULL && $x_input_bus == NULL){
			
			$consulta="SELECT id_fam, mt_name, mt_ap, mt_mat, mt_date, mt_age, ft_dom, ft_cd, ft_col, ft_cp, ft_tel, mt_cel, mt_email, mt_ocup, mt_emp, mt_giro, mt_pst, mt_ant, mt_empds, mt_porcen FROM inf_familia";
		} else if($x_tip_bus == "name"){
				 
		 	//$consulta="SELECT * FROM inf_father where ft_name LIKE'%".$x_input_bus."%'"; 
			$consulta="SELECT * FROM `inf_familia` where concat_ws(' ',`mt_name`,`mt_ap`,`mt_mat`) LIKE '%".utf8_decode($x_input_bus)."%'";
				
		}else if($x_tip_bus == "fam"){
				$consulta="SELECT * FROM inf_familia where id_fam LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "tel"){
			
			$consulta="SELECT * FROM inf_familia where ft_tel LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "cel"){
			
			$consulta="SELECT * FROM inf_familia where mt_cel LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "email"){
			
			$consulta="SELECT * FROM inf_familia where mt_email LIKE'%".$x_input_bus."%'"; 
			
		}
		
	}else if(isset($x_resg_busc)){
		
		$consulta="SELECT * FROM inf_familia";
		
	}else{
		
		$consulta="SELECT * FROM inf_familia";
		
	}

$datos=mysql_query($consulta);
$num_rows=mysql_num_rows($datos);
$rows_per_page= 100;
$lastpage= ceil($num_rows / $rows_per_page);
$page=(int)$page;
$band_del=0;
if($page > $lastpage)
{
	$page= $lastpage;
}

if($page < 1)
{
	$page=1;
}

$limit= 'LIMIT '. ($page -1) * $rows_per_page . ',' .$rows_per_page;
$consulta .=" $limit";
$peliculas=mysql_query($consulta);

////

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
        
        <script type="text/javascript">
			function opcion(id,name, usuario)
			{
				var answer = confirm("Esta seguro de querer eliminar a la madre de familia: "+ name)
			 
				if (answer)
				{
					window.location="actions/mt_del.php?id="+ id +"&usuario="+ usuario;
					 
				}else { 
				 return false; 
				}
				
			}
		</script>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
                <!-- Notification boxes -->
                	<?php
					if($band_error_msg=="display"){
						
					?>
                    <span class="notification n-success"> Usuario <?php echo $_SESSION["del_mt_name"];?> ha sido eliminado exitosamente.</span>
                    <?php
						unset($_SESSION["del_mt_name"]);
					}
					?>	
                    
                    <?php
					if($band_modf_msg=="display_mod"){
						
					?>
                    <span class="notification n-success"> Usuario <?php echo $_SESSION["mod_mt_name"];?> ha sido modificado exitosamente.</span>
                    <?php
					unset($_SESSION["mod_mt_name"]);
					}
					?>
                
              <div class="bottom-spacing">
                
                
                <div class="float-left">
                  		<form action="inf_mt.php" method="post" enctype="multipart/form-data">
                        <table width="514" border="0" cellspacing="5">
                          <tr>
                          	<td width="137">
                            	<select id="tip_bus" name="tip_bus">
                                    <option value="" selected="selected">Selecciona..</option>
                                    <option value="fam">Familia</option>
                                    <option value="name">Nombre</option>
                                    <option value="tel">Teléfono</option>
                                    <option value="cel">Celular</option>
                                    <option value="email">Email</option>
                              	</select>
                            </td>
                            <td width="142"><input name="input_bus" id="input_bus" type="text" /></td>
                            <td width="100">
                            <input class="submit-green" style="width:90px; height:30px"  name="busc" id="busc" type='submit'  value="Buscar"/>
                            </td>
                            <td width="102">
                            <?php if(isset($x_busc) && $x_tip_bus != NULL && $x_input_bus != NULL){ ?>
                         	 
                           <input class="submit-green" style="width:90px; height:30px"  name="resg_busc" id="resg_busc" type='submit'  value="Regresar" />
                            
							<?php }?>
                            </td> 
                          </tr>
                        </table>
					</form>
                  		
                		
                </div>
                  
                <div class="float-right">
                <table width="224" border="0" cellspacing="5">
                          <tr>
                          	<td width="139">
                            	<a href="import_mt.php">
                                 
                                 <input class="submit-green" style="width:90px; height:30px"  name="enviar" id="enviar" type='submit'  value="Importar" />
                               
                              	</a>
                            </td>
                            <td width="155"><a href="actions/mt_ex.php">
                     
                            <input class="submit-green" style="width:90px; height:30px"  name="enviar" id="enviar" type='submit'  value="Exportar" />
                     
                  </a></td>
                           
                          </tr>
                        </table>
           		  
                           
                </div>
              </div>
          
                
                <!-- Example table -->
                <div class="module">
                	<h2><span>Información Madres de Familia</span></h2>
                    
                    <div class="module-table-body">
                    	<form action="">
                        <table id="myTable" class="tablesorter">
                        	
                                <tr>
                                    <th width="3%">#</th>
                                    <th width="5%">Familia</th>
                                    <th width="25%">Nombre</th>
                                    <th width="12%">Teléfono</th>
                                    <th width="17%">Celular</th>
                                    <th width="28%">Correo electrónico</th>
                                    <th width="10%">Opciones</th>
                                </tr>
                            <?php
								if($page == 1){
								
								$band_id=1;
								
								}else{
								
								$band_id=($page * $rows_per_page) - ($rows_per_page) + 1;
								
								}
								
								while ($row = mysql_fetch_array($peliculas)){
								$x_id_fam=$row["id_fam"];	
								$x_mt_name=$row["mt_name"];
								$x_mt_ap=$row["mt_ap"];
								$x_mt_mat=$row["mt_mat"];
								$x_mt_tel=$row["ft_tel"];
								$x_mt_cel=$row["mt_cel"];
								$x_mt_email=$row["mt_email"];
								$usuario=$row["usuario"];
								
								if($x_mt_cel == NULL || $x_mt_cel == 0){ $x_mt_cel="";}
								if($x_mt_tel == NULL || $x_mt_tel == 0){ $x_mt_tel="";}
								if($x_mt_email == NULL){ $x_mt_email="";}
								
								$name_del_mt=utf8_encode($x_mt_name)." ".utf8_encode($x_mt_ap)." ".utf8_encode($x_mt_mat);
								
								?>
                            <tbody>
                                <tr>
             <td class="align-center"><?php echo $band_id;?></td>
             <td class="align-center"><?php echo $usuario;?></td>
                                    
                                    <td><?php echo utf8_encode($x_mt_name)." ".utf8_encode($x_mt_ap)." ".utf8_encode($x_mt_mat);?></td>                                    <td><?php echo $x_mt_tel;?></td>
                                    <td><?php echo $x_mt_cel;?></td>
                                    <td><?php echo utf8_encode($x_mt_email);?></td>
                                    <td>
                                    <a href="mt_perfil.php?id=<?php echo $x_id_fam;?>&usuario=<?php echo $usuario;?>"><img src="images/user-female.gif" width="16" height="16" alt="Perfil" /></a>
                                    	<a href="mt_update.php?id=<?php echo $x_id_fam;?>&usuario=<?php echo $usuario;?>"><img src="images/bin.gif" width="16" height="16" alt="delete" /></a>
                                     <a onclick="opcion(<?php echo $x_id_fam." , '".$name_del_mt."', '".$usuario."'";?>);"><img src="images/minus-circle.gif" width="16" height="16" alt="not published" /></a>
                                    </td>
                                </tr>
                            </tbody>
                          <?php $band_id++; }  ?>
                        </table>
                        </form>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->
                
                <div style="float:left;">
                Ver Perfil: <img src="images/user-female.gif" width="16" height="16" alt="Perfil" /> &nbsp; &nbsp; Modificar: <img src="images/bin.gif" width="16" height="16" alt="Modificar" /> &nbsp; &nbsp; Eliminar: <img src="images/minus-circle.gif" width="16" height="16" alt="delete" />
                </div>
                
               <div class="pagination">           
                		 <?php
							//UNA VEZ Q MUESTRO LOS DATOS TENGO Q MOSTRAR EL BLOQUE DE PAGINACIÓN SIEMPRE Y CUANDO HAYA MÁS DE UNA PÁGINA
							if($num_rows != 0)
							{
							   $nextpage= $page +1;
							   $prevpage= $page -1;
						?>
						<?php
						//SI ES LA PRIMERA PÁGINA DESHABILITO EL BOTON DE PREVIOUS, MUESTRO EL 1 COMO ACTIVO Y MUESTRO EL RESTO DE PÁGINAS
								  if($page == 1) 
								   {
									?>
                       <!-- <a href="" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>-->
                        <div class="numbers">
                            <span>Paginas:</span> 
                            <span class="current">1</span> 
                            <span>|</span>
                            
                            <?php
							  for($i= $page+1; $i<= $lastpage ; $i++)
							  {
							?> 
                            	<a href="inf_mt.php?page=<?php echo $i;?>"><?php echo $i;?></a> 
                            	<span>|</span>
                            <?php 
							  }
							?>
                            
                            <?php
                             //Y SI LA ULTIMA PÁGINA ES MAYOR QUE LA ACTUAL MUESTRO EL BOTON NEXT O LO DESHABILITO
										if($lastpage >$page )
										{
									?>    
                               </div>        
                       	  <a href="inf_mt.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                         <?php
										}else{
									?>
                       			</div>
                                   			<!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                        <?php
										}
									} else{
									//EN CAMBIO SI NO ESTAMOS EN LA PÁGINA UNO HABILITO EL BOTON DE PREVIUS Y MUESTRO LAS DEMÁS
								?>
                       <a href="inf_mt.php?page=<?php echo $prevpage;?>" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>
                      <div class="numbers"> 
                      <span>Paginas:</span>
                      
                        <?php
										 for($i= 1; $i<= $lastpage ; $i++)
										 {
													   //COMPRUEBO SI ES LA PÁGINA ACTIVA O NO
											if($page == $i)
											{
								?>     
                                  
                            
                                <span class="current"><a><?php echo $i;?></a></span><span>|</span>
                                
                                <?php
											}
											else
											{
								?>
                                
                                    
                                <a href="inf_mt.php?page=<?php echo $i;?>"><?php echo $i;?></a>
                                <span>|</span>
                                
                                <?php
											}
										}
								?>
                                
                                <?php		
										 //Y SI NO ES LA ÚLTIMA PÁGINA ACTIVO EL BOTON NEXT     
										if($lastpage >$page )
										{   
								?>   
                            </div>   
                       <a href="inf_mt.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                      
                                <?php
										}
									 else
										{
								?>  
                                   </div> 
                                   <!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                                   
								   <?php
										}
								}     
							  }
							?>
                            
                       <div style="clear: both;"></div> 
                     </div>
                
                

                
			</div> <!-- End .grid_12 -->

          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>