<?php
session_start();
if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
	if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
include("z_script/db_class.php");	
mysql_select_db($bd_becas, $link);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <script type="text/javascript" src="js/vald_info.js"></script>
		<?php
            include_once("z_script/header.php");
        ?>
        		
		
        <style type="text/css">
		div.module table td  {
		background-color: #ffffff;
		padding: 5px;
		border-right: 0px solid #ffffff;
		}

		div.module table {
			width: 0%;
			margin: 0 0 10px 0;
			border-left: 0px solid #d9d9d9;
			border-bottom: 0px solid #d9d9d9;
			
			}
		</style>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
	<div class="container_12">
            
            <div style="clear:both;"></div>
            
             <div class="grid_12">
                <div class="module">
    <form name="alta_usuarios" method="post"  enctype="multipart/form-data" action="actions/nuevo_alumno.php">
                  <h2><span>Dar de Alta un Nuevo Alumno</span></h2>
                        
      <div class="module-table-body">
                     <table width="500" border="0" style="border-left: 0px solid #d9d9d9; border-bottom: 0px solid #d9d9d9; border-right: 0px sold #ffffff; margin-left:10px; margin-top:10px;">
                        <tr>
                          <td width="132">Familia:</td>
                            <td><select id="familia" name="familia">
                                	<option value="" selected="selected">Selecciona...</option>
									<?								
								$rs = mysql_query ("select * from user_fam order by id_fam",$link) 
						or die ("select * from user_fam order by id_fam".mysql_error());
						mysql_query("SET CHARACTER SET 'utf8'");
								while($r=mysql_fetch_array($rs)){
									$nombre_familia=$r['nombre_familia'];
									$familia=$r['id_fam'];
									?>
									<option value="<? echo $familia;?>" ><? echo $familia.' -- '.utf8_encode($nombre_familia)?> </option>
									<?
								}
									?>
                               
                            	</select>'</td>
						  
						  
<!--                          <td width="159"><input id="familia" name="familia" /></td> -->
                          <td><div id="msg_red_<?php echo $x_cont_msg=0; ?>" class="notification-input ni-error" style="display:none;"></div> </td>
                        </tr>
                                            
                        <tr>
                          <td width="132">Matrícula:</td>
                          <td width="159"><input id="matricula" name="matricula" maxlength="10" /></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        <tr>
                          <td width="132">Nombre:</td>
                          <td width="159"><input id="nombre" name="nombre" type="text" value=""/></td>
                          <td width><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        <tr>
                          <td>Apellido paterno:</td>
                          <td><input name="apellidop" id="apellidop" type="text" value=""/></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
						<tr >
                            <td> Apellido materno: </td>
                            <td><input type="text" id="apellidom" name="apellidom" value=""/></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  
						  <tr>
                            <td> Sexo: </td>
                            <td><select id="sexo" name="sexo">
                                	<option value="" selected="selected">Selecciona...</option>									
									<option value="M" >Masculino</option>
									<option value="F" >Femenino</option>	                               
                            	</select></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  
						   <tr>
                            <td> Colegio: </td>
                            <td><select id="colegio" name="colegio">
                                	<option value="" selected="selected">Selecciona...</option>
									<?								
								$rs = mysql_query ("select * from colegios",$link) 
						or die ("select * from colegios".mysql_error());
								while($r=mysql_fetch_array($rs)){
									$colegio=$r['nombre'];
									$base=$r['nombre_bd'];
									?>
									<option value="<? echo $colegio;?>" ><? echo $colegio?> </option>
									<?
								}
									?>
                               
                            	</select></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  
						  <tr >
                            <td> Carrera: </td>
                            <td>
							<select id="carrera" name="carrera">
                                	<option value="" selected="selected">Selecciona...</option>
									<?								
								$rs = mysql_query ("select * from carreras",$link) 
						or die ("select * from carreras".mysql_error());
						mysql_query("SET CHARACTER SET 'utf8'");
								while($r=mysql_fetch_array($rs)){
									$carrera=$r['carrera'];
									$ncarrera=$r['nombre'];
									?>
									<option value="<?=$carrera ?>" ><?=$ncarrera?> </option>
									<?
								}
									?>
                               
                            	</select>
							</td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  <tr >
                            <td> Semestre: </td>
                            <td>
							<select id="grado" name="grado">
                    <option value="" selected="selected">Selecciona</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
				    <option value="3" >3</option> 
					<option value="4" >4</option>
				    <option value="5" >5</option>
					<option value="6" >6</option>
					<option value="7" >7</option>
					<option value="8" >8</option>
					<option value="9" >9</option>
					<option value="10" >10</option>
					<option value="11" >11</option>
					<option value="12" >12</option>
                    </select>
							<!--<input type="text" id="grado" name="grado" value=""/>-->
							</td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  <tr >
						  <tr >
                            <td> Grupo: </td>
                            <td><input type="text" id="grupo" name="grupo" value=""/></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  
						   <tr>
                            <td> Turno: </td>
                            <td><select id="turno" name="turno">
                                	<option value="" selected="selected">Selecciona...</option>									
									<option value="M" >Matutino</option>
									<option value="V" >Vespertino</option>	                               
                            	</select></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>	
						  
						  <tr >
                            <td> Nuevo ingreso: </td>
                            <td><select id="ningreso" name="ningreso">
                                	<option value="" selected="selected">Selecciona...</option>
									<option value="S" >Si</option>
									<option value="N" >No</option>                               
                            	</select>'</td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>					  
						 						  
						  <tr>
                            <td> País: </td>
                            <td><select id="pais" name="pais">
                                	<option value="" selected="selected">Selecciona...</option>
									<?	
								mysql_select_db($bd_sie,$link);							
								$rs = mysql_query ("select * from paises",$link) 
						or die ("select * from paises".mysql_error());
								while($r=mysql_fetch_array($rs)){
									$pais=$r['pais'];
									$nombre=$r['nombre'];
									?>
									<option value="<? echo $pais;?>" ><? echo $nombre?> </option>
									<?
								}
								mysql_select_db($bd_becas,$link);
									?>
                               
                            	</select></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
						  					  					  						  
						  
						  <tr >
                            <td> Usuario/Familia: </td>
                            <td><select id="usuario" name="usuario">
                                	<option value="" selected="selected">Selecciona...</option>
									<?								
								$rs = mysql_query ("select * from user_fam order by id_fam",$link) 
						or die ("select * from user_fam order by id_fam".mysql_error());
						mysql_query("SET CHARACTER SET 'utf8'");
								while($r=mysql_fetch_array($rs)){
									$nombre_familia=$r['nombre_familia'];
									$familia=$r['id_fam'];
									$us=$r['fam_user'];
									?>
									<option value="<? echo $us;?>" ><? echo $us.' -- '.$nombre_familia?> </option>
									<?
								}
									?>
                               
                            	</select>'</td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                      
                      </table>
                      <p>
                      <fieldset>
                            <input class="submit-green" style="width:100px; height:30px; float:left; margin-left:12px;"  name="enviar" id="enviar" type='submit'  suvalue="Aceptar" />
                            </fieldset>
                      
                      </p>
                  
      </div> 
                <!-- End .module-body --><!-- End .container_12 -->
	</form>
    </div>
    </div>
    <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
        <?php include_once("z_script/footer.php") ?>   
        
</body>
</html>