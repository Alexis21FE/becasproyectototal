<?php 

session_start();

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador" ) // If the user IS NOT logged in, forward them back to the login page
{
	if($_SESSION["tipo_priv"] != "Colegio" ) // If the user IS NOT logged in, forward them back to the login page
	{	
		if($_SESSION["tipo_priv"] != "Coordinador" ) // If the user IS NOT logged in, forward them back to the login page
		{
			header("location: index.php");
		}
	}
}

/* Librerias and variables */
include ('../z_script/fpdf/fpdf.php');
include("z_script/db_class.php");
$fam_ap="";
$fam_am="";
$fam_al="";
$pingreso="";
$x_date_term="";
$fam_id = $_GET["i"];
$alum_selec = $_GET["a"];
$usuario = $_GET["usuario"];

$ciclob=0;
$sql="SELECT * FROM solc_term WHERE usuario='$usuario'"; 
 $result=$pdo->query($sql);
foreach ($result as $row){ 
		$ciclob 	= $row["ciclo"]; 
}
$ciclob = $_GET["ciclo"];

$sql4="SELECT * FROM solc_term WHERE alum_mat='".$alum_selec."' and usuario='$usuario' and ciclo=$ciclob"; 
$result4=$pdo->query($sql4);

foreach ($result4 as $row4){
	$x_date_term=$row4["date_term"];
}

$sql_al="SELECT * FROM inf_alum WHERE alum_mat=".$alum_selec." and usuario='$usuario'"; 
$result_al=$pdo->query($sql_al);

foreach ($result_al as $row){ 
			$fam_al = $row["alum_name"];
			$fam_ap = $row["alum_ap"]; 
			$fam_am = $row["alum_am"];
}

class PDF extends FPDF
{
// Cargar los datos

}

$pdf = new PDF();
$pdf=new PDF('P','mm','Letter');

// Carga de datos
$pdf->SetFont('Arial','',10);
$pdf->AddPage();
$pdf->Image("../images/sol_soc_til.png" , 31 ,5, 150 , 40 , "PNG");
$pdf->Ln(35);
$pdf->Cell(41,0,'Ciclo: '.$ciclob ,0,0,'C');
$pdf->Ln(2);

$pdf->Cell(60,5,'Familia: '.$fam_ap.' '.$fam_am ,0,0,'C');
$pdf->Cell(75,5,'Alumno: '.$fam_al.' '.$fam_ap.' '.$fam_am ,0,0,'C');	
$pdf->Cell(60,5,'Fecha: '.$x_date_term,0,1,'C');
$pdf->Ln(5);

$sql_dep="SELECT * FROM dep_econom WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_dep=$pdo->query($sql_dep);
$num_rows_dep=$result_dep->rowCount();

if($num_rows_dep >= 1){
	
	$pdf->Cell(200,1,$pdf->Image("images/soc_dep.png" , ($pdf->GetX() - 5) ,$pdf->GetY(), 200 , 6),0,1,'L');
//$pdf->Image("images/soc_dep.png" , 4 ,50, 200 , 6 , "PNG");
	
	$pdf->Ln(5);
$pdf->SetFont('Arial','',8);
//$pdf->Cell(50,5,utf8_decode('NOTA: NO incluír alumnos solicitantes').'',0,1,'L');
	
    foreach ($result_dep as $row){
			$dep_nam = $row['nomb_comp'];
			$dep_eda = $row['dep_edad'];
			$dep_par = $row['dep_parent'];
			$dep_ocu = $row['dep_ocup'];
			$dep_int = $row['dep_inst_trab'];
			$dep_gra = $row['dep_grado_ing'];
			$dep_por = $row['dep_porc_beca'];
			
			if($dep_gra == 0){$dep_gra ="";}
			
	if($dep_por == 0){$dep_por ="";}
	
	$pdf->Ln(2);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(50,5,'Nombre Completo:',0,'L');
	$pdf->Cell(120,5,utf8_decode($dep_nam),'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,'Edad:',0,0,'L');
	$pdf->Cell(17,5,$dep_eda,'B',0,'C');
	$pdf->Cell(13,5,utf8_decode('años'),0,1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,'Parentesco:',0,0,'L');
	$pdf->Cell(47,5,utf8_decode($dep_par),'B',0,'L');
	$pdf->Cell(13,5,'',0,0,'L');
	$pdf->Cell(20,5,utf8_decode('Ocupación:'),0,0,'L');
	$pdf->Cell(40,5,utf8_decode($dep_ocu),'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('Institución en la que estudia o ').'',0,0,'L');
	$pdf->Cell(107,5,utf8_decode($dep_int),'B',1,'L');
	$pdf->Cell(107,3,utf8_decode('trabaja:').'',0,1,'L');
	$pdf->Ln(2);
	if($dep_ocu=='estudiante'){
		$pdf->Cell(50,5,'Grado a ingresar:',0,'L');
		$pdf->Cell(27,5,$dep_gra,'B',1,'L');
		$pdf->Ln(2);
		$pdf->Cell(50,5,'% de beca (si cuenta con ella):',0,'L');
		$pdf->Cell(27,5,$dep_por,'B',1,'L');
		$pdf->Ln(2);
	}		
	}
	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7
//////dependientes economicos hijos////////////////////////////////////////////////////////////////////////////////////////////
//if($x_tip_bec=="bec_intof"){
//if($x_tip_bec=="bec_intof"){
$pdf->Cell(200,1,$pdf->Image("images/soc_bie.png" , ($pdf->GetX() - 5) ,$pdf->GetY(), 200 , 6),0,1,'L');
//$pdf->Image("images/soc_bie.png" , 4 ,110, 200 , 6 , "PNG");

/// Datos Bienes
$sql_bie="SELECT * FROM bien_pat WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_bie=$pdo->query($sql_bie);
$num_rows_bie=$result_bie->rowCount();
$pdf->Ln(5);
if($num_rows_bie == 1){
	foreach($result_bie as $row){
			$bie_cas = $row['casa_prop'];
			$bie_hip = $row['es_hipot'];
			$bie_ren = $row['es_rent'];
			$bie_con = $row['tien_cond'];
			$bie_ter = $row['tien_terr'];
			$bie_cca = $row['tien_cas_camp'];
			$bie_oca = $row['tien_otra_prop'];
			$bie_per = $row['pert_club'];
			$bie_clu = $row['club'];
	}
	$pdf->Cell(50,5,utf8_decode('¿Tiene casa propia?'),0,'L');
	$pdf->Cell(27,5,$bie_cas,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Es hipotecada?'),0,'L');
	$pdf->Cell(27,5,$bie_hip,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Es rentada?'),0,'L');
	$pdf->Cell(27,5,$bie_ren,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Tiene algún condominio?'),0,'L');
	$pdf->Cell(27,5,$bie_con,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Tiene algún terreno?'),0,'L');
	$pdf->Cell(27,5,$bie_ter,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Tiene casa de campo?'),0,'L');
	$pdf->Cell(27,5,$bie_cca,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Tiene otra propiedad?'),0,'L');
	$pdf->Cell(27,5,$bie_oca,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿Pertenece a algún club?'),0,'L');
	$pdf->Cell(27,5,$bie_per,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('¿A qué club?'),0,'L');
	$pdf->Cell(47,5,$bie_clu,'B',1,'L');

}

$pdf->Ln(5);

if($num_rows_dep == 2){ 
//$pdf->AddPage();
$pdf->SetFont('Arial','',10);

}

//----------------------------------------------------------------------------------------------------------------//

/// Datos Veh

$sql_veh="SELECT * FROM vehiculos WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_veh=$pdo->query($sql_veh);
$num_rows_veh=$result_veh->rowCount();
$band_veh=1;

if($num_rows_veh >= 1){
	
	$pdf->Cell(200,1,$pdf->Image("images/soc_veh.png" , ($pdf->GetX() - 5) ,$pdf->GetY(), 200 , 6),0,1,'L');
//$pdf->Image("images/soc_veh.png" , 4 ,181, 200 , 6 , "PNG");		
	$pdf->Ln(5);
	$pdf->Cell(50,5,utf8_decode('Cantidad de vehículos:'),0,'L');
	$pdf->Cell(10,5,$num_rows_veh,'B',1,'C');
	$pdf->Ln(2);

	
	foreach ($result_veh as $row){ 
			$veh_mar 	= $row["marca"];
			$veh_nam 	= $row["nombre"];
			$veh_mody 	= $row["mod_year"]; 
	
	$pdf->Cell(50,5,utf8_decode('Vehículo '.$band_veh),0,1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('Marca:'),0,'L');
	$pdf->Cell(37,5,utf8_decode($veh_mar),'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('Propietario:'),0,'L');
	$pdf->Cell(37,5,utf8_decode($veh_nam),'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(50,5,utf8_decode('Modelo (año):'),0,'L');
	$pdf->Cell(17,5,$veh_mody,'B',1,'L');
	$pdf->Ln(4);
	$band_veh++;
	
	}
} 

$pdf->Ln(5);

if( $num_rows_veh == 4 && $num_rows_dep == 10){ 
$pdf->AddPage();
$pdf->SetFont('Arial','',10);

}
//$pdf->Image("images/soc_ing.png" , $this->GetX() ,$this->GetY(), 200 , 6 , "PNG");
$pdf->Cell(200,1,$pdf->Image("images/soc_ing.png" , ($pdf->GetX() - 5) ,$pdf->GetY(), 200 , 6),0,1,'L');
/// Datos Ingresos 
$pdf->Ln(8);

$sql_ing="SELECT * FROM ingre_mensu WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_ing=$pdo->query($sql_ing);
$num_rows_ing=$result_ing->rowCount();

if($num_rows_ing == 1){
	foreach ($result_ing as $row){
			$ing_fat = $row['ing_fat'];
			$ing_mot = $row['ing_mot'];
			$ing_val = $row['ing_val_desp'];
			$ing_oth = $row['other_Ing'];
			$ing_tol = $row['ing_total'];
	}
		
	if($ing_fat != 0){ $ing_fat="$ ".$ing_fat;}else{ $ing_fat=""; }
	if($ing_mot != 0){ $ing_mot="$ ".$ing_mot;}else{ $ing_mot=""; }
	if($ing_val != 0){ $ing_val="$ ".$ing_val;}else{ $ing_val=""; }
	if($ing_oth != 0){ $ing_oth="$ ".$ing_oth;}else{ $ing_oth=""; }
	
	$pdf->Cell(60,5,'Ingresos del padre (después de '.'',0,0,'L');
	$pdf->Cell(27,5,$ing_fat,'B',1,'L');
	$pdf->Cell(107,3,'impuestos):'.'',0,1,'L');
	$pdf->Ln(2);
	$pdf->Cell(60,5,'Ingresos de la madre (después de '.'',0,0,'L');
	$pdf->Cell(27,5,$ing_mot,'B',1,'L');
	$pdf->Cell(107,3,'(impuestos):'.'',0,1,'L');
	$pdf->Ln(2);
	$pdf->Cell(60,5,'Ingresos en vales de despensa:',0,'L');
	$pdf->Cell(27,5,$ing_val,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(60,5,'Otros ingresos:',0,'L');
	$pdf->Cell(27,5,$ing_oth,'B',1,'L');
	$pdf->Ln(2);
	$pdf->Cell(60,5,'Ingresos Totales:',0,'L');
	$pdf->Cell(27,5,'$ '.$ing_tol,'B',1,'L');

}

$pdf->Ln(7);

//-----------------------------------------------//

$pdf->Cell(200,1,$pdf->Image("images/soc_gast.png" , ($pdf->GetX() - 5) ,$pdf->GetY(), 200 , 6),0,1,'L');
//$pdf->Image("images/soc_gast.png" , 4 ,64, 200 , 6 , "PNG");
/// Datos Gast

$sql_gast="SELECT * FROM gastos WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_gast	= $pdo->query($sql_gast);
$num_rows_gast	= $result_gast->rowCount();

$pdf->Ln(8);

if($num_rows_gast == 1){
	foreach ($result_gast as $row){
			$gast_ali = $row['alimentacion'];
			$gast_ren = $row['renta'];
			$gast_pres = $row['prestamos'];
			$gast_ene = $row['ener_elect'];
			$gast_agu = $row['agua'];
			$gast_gas = $row['gas'];
			$gast_aut = $row['autos'];
			$gast_hip = $row['hipoteca'];
			$gast_pre = $row['predial'];
			$gast_tel = $row['telefono'];
			$gast_tvc = $row['tv_cable'];
			$gast_int = $row['internet'];
			$gast_col = $row['coleg'];
			$gast_ext = $row['clases_extra'];
			$gast_dom = $row['serv_dom'];
			$gast_lib = $row['libros'];
			$gast_div = $row['diversion'];
			$gast_med = $row['medicos'];
			$gast_den = $row['dentista'];
			$gast_ves = $row['vestido'];
			$gast_seg = $row['seguros'];
			$gast_via = $row['viajes'];
			$gast_oth = $row['otros_gast'];
			$gast_des_oth = $row['des_otr_gas'];
			$gast_tol = $row['gast_total'];
			
	}
		
	if($gast_ali != 0){ $gast_ali="$ ".$gast_ali;}else{ $gast_ali=""; }
	if($gast_ren != 0){ $gast_ren="$ ".$gast_ren;}else{ $gast_ren=""; }
	if($gast_pres != 0){ $gast_pres="$ ".$gast_pres;}else{ $gast_pres=""; }
	if($gast_ene != 0){ $gast_ene="$ ".$gast_ene;}else{ $gast_ene=""; }
	if($gast_agu != 0){ $gast_agu="$ ".$gast_agu;}else{ $gast_agu=""; }
	if($gast_gas != 0){ $gast_gas="$ ".$gast_gas;}else{ $gast_gas=""; }
	if($gast_aut != 0){ $gast_aut="$ ".$gast_aut;}else{ $gast_aut=""; }
	if($gast_hip != 0){ $gast_hip="$ ".$gast_hip;}else{ $gast_hip=""; }
	if($gast_pre != 0){ $gast_pre="$ ".$gast_pre;}else{ $gast_pre=""; }
	if($gast_tel != 0){ $gast_tel="$ ".$gast_tel;}else{ $gast_tel=""; }
	if($gast_tvc != 0){ $gast_tvc="$ ".$gast_tvc;}else{ $gast_tvc=""; }
	if($gast_int != 0){ $gast_int="$ ".$gast_int;}else{ $gast_int=""; }
	if($gast_col != 0){ $gast_col="$ ".$gast_col;}else{ $gast_col=""; }
	if($gast_ext != 0){ $gast_ext="$ ".$gast_ext;}else{ $gast_ext=""; }
	if($gast_dom != 0){ $gast_dom="$ ".$gast_dom;}else{ $gast_dom=""; }
	if($gast_lib != 0){ $gast_lib="$ ".$gast_lib;}else{ $gast_lib=""; }
	if($gast_div != 0){ $gast_div="$ ".$gast_div;}else{ $gast_div=""; }
	if($gast_med != 0){ $gast_med="$ ".$gast_med;}else{ $gast_med=""; }
	if($gast_den != 0){ $gast_den="$ ".$gast_den;}else{ $gast_den=""; }
	if($gast_ves != 0){ $gast_ves="$ ".$gast_ves;}else{ $gast_ves=""; }
	if($gast_seg != 0){ $gast_seg="$ ".$gast_seg;}else{ $gast_seg=""; }
	if($gast_via != 0){ $gast_via="$ ".$gast_via;}else{ $gast_via=""; }
	if($gast_oth != 0){ $gast_oth="$ ".$gast_oth;}else{ $gast_oth=""; }
	if($gast_des_oth != ""){ $gast_des_oth=$gast_des_oth;}else{ $gast_des_oth=""; }
	if($gast_tol != 0){ $gast_tol="$ ".$gast_tol;}else{ $gast_tol=""; } 		
	
	$pdf->Cell(40,5,'Alimentación:',0,0,'L');
	$pdf->Cell(27,5,$gast_ali,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Colegiaturas:',0,0,'L');
	$pdf->Cell(27,5,$gast_col,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,'Renta:',0,0,'L');
	$pdf->Cell(27,5,$gast_ren,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Clases o cursos extracurricuales:',0,0,'L');
	$pdf->Cell(27,5,$gast_ext,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,'Préstamos*:',0,0,'L');
	$pdf->Cell(27,5,$gast_pres,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Servicio doméstico:',0,0,'L');
	$pdf->Cell(27,5,$gast_dom,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,'Energía Eléctrica:',0,0,'L');
	$pdf->Cell(27,5,$gast_ene,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Libros:',0,0,'L');
	$pdf->Cell(27,5,$gast_lib,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,'Agua Potable:',0,0,'L');
	$pdf->Cell(27,5,$gast_agu,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Diversión:',0,0,'L');
	$pdf->Cell(27,5,$gast_div,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Gas:'),0,0,'L');
	$pdf->Cell(27,5,$gast_gas,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,'Médicos:',0,0,'L');
	$pdf->Cell(27,5,$gast_med,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Autos:'),0,0,'L');
	$pdf->Cell(27,5,$gast_aut,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Dentista:'),0,0,'L');
	$pdf->Cell(27,5,$gast_den,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Hipoteca:'),0,0,'L');
	$pdf->Cell(27,5,$gast_hip,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Vestido:'),0,0,'L');
	$pdf->Cell(27,5,$gast_ves,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Predial:'),0,0,'L');
	$pdf->Cell(27,5,$gast_pre,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Seguros:'),0,0,'L');
	$pdf->Cell(27,5,$gast_seg,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,'Teléfono:',0,0,'L');
	$pdf->Cell(27,5,$gast_tel,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Viajes:'),0,0,'L');
	$pdf->Cell(27,5,$gast_via,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Cable:'),0,0,'L');

	$pdf->Cell(27,5,$gast_tvc,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Otros gastos:'),0,0,'L');
	$pdf->Cell(27,5,$gast_oth,'B',1,'C');
	$pdf->Ln(2);
	$pdf->Cell(40,5,utf8_decode('Internet:'),0,0,'L');
	$pdf->Cell(27,5,$gast_int,'B',0,'C');
	$pdf->Cell(20,5,'',0,0,'L');
	$pdf->Cell(55,5,utf8_decode('Gastos totales:'),0,0,'L');
	$pdf->Cell(27,5,$gast_tol,'B',0,'C');
	$pdf->Ln(10);
	$pdf->Cell(55,5,utf8_decode('Desglose de otros gastos:'),0,1,'L');
	$pdf->Ln(3);
	$pdf->MultiCell(190,6,utf8_decode($gast_des_oth),1,'L',0);
	
	
}


$pdf->Ln(10);
//---------------------------------------------------------------//


if($num_rows_veh == 2 && $num_rows_dep == 2 || $num_rows_veh == 1 && $num_rows_dep == 2 || $num_rows_veh == 4 && $num_rows_dep == 6){ 
//$pdf->AddPage();
$pdf->SetFont('Arial','',10);

}

$pdf->Cell(200,1,$pdf->Image("images/soc_mot.png" , ($pdf->GetX() - 3) ,$pdf->GetY(), 200 , 6),0,1,'L');

//$pdf->Image("images/soc_mot.png" , 7 ,184, 200 , 6 , "PNG");

/// Datos Motivo
$pdo->query("SET CHARACTER SET 'utf8'");
$sql_mot="SELECT * FROM motiv_solic WHERE usuario='$usuario' and ciclo=$ciclob"; 
$result_mot		= $pdo->query($sql_mot);
$num_rows_mot	= $result_mot->rowCount();

$pdf->Ln(12);

if($num_rows_mot == 1){
	foreach ($result_mot as $row){
			$mot_mot = $row['motivo_sol'];
	}
    
	$pdf->MultiCell(195,6,utf8_decode($mot_mot),1,'L',0);
}

//}//fin if beca oficial

$pdf->Ln(2);


$pdf->Output();
?>
