<?php
session_start();
if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
	if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html >
	<head>
    <script type="text/javascript" src="js/vald_info.js"></script>
		<?php
            include_once("z_script/header.php");
        ?>
        		
		
        <style type="text/css">
		div.module table td  {
		background-color: #ffffff;
		padding: 5px;
		border-right: 0px solid #ffffff;
		}

		div.module table {
			width: 0%;
			margin: 0 0 10px 0;
			border-left: 0px solid #d9d9d9;
			border-bottom: 0px solid #d9d9d9;
			
			}
		</style>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
	<div class="container_12">
            
            <div style="clear:both;"></div>
            
             <div class="grid_12">
                <div class="module">
    <form name="alta_usuarios" method="post"  enctype="multipart/form-data" action="actions/users_add.php">
                  <h2><span>Dar de Alta un Nuevo Usuario</span></h2>
                        
      <div class="module-table-body">
                     <table width="500" border="0" style="border-left: 0px solid #d9d9d9; border-bottom: 0px solid #d9d9d9; border-right: 0px sold #ffffff; margin-left:10px; margin-top:10px;">
                        <tr>
                          <td width="132">Nombre:</td>
                          <td width="159"><input id="name" name="name" /></td> 
                          <td><div id="msg_red_<?php echo $x_cont_msg=0; ?>" class="notification-input ni-error" style="display:none;"></div> </td>
                        </tr>
                                            
                        <tr>
                          <td width="132">Apellido Paterno:</td>
                          <td width="159"><input id="ape_pat" name="ape_pat" /></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        <tr>
                          <td width="132">Apellido Materno:</td>
                          <td width="159"><input id="ape_mat" name="ape_mat" type="text" value=""/></td>
                          <td width><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        <tr>
                          <td>Colegio:</td>
                          <td><input name="us_coleg" id="us_coleg" type="text" value=""/></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        <tr>
                          <td>Privilegios:</td>
                          <td><select name="tip_priv" id="tip_priv">
                            		<option value="">Selecciona</option>
                                    <option value="Administrador" >Administrador</option>
                                   
                                   <option value="Colegio">Colegio</option>
                                    
                            	</select></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                       
                          <tr >
                            <td> Usuario: </td>
                            <td><input type="text" id="usuario" name="usuario" value=""/></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                      
                        <tr>
                          <td height="27">Contraseña:</td>
                          <td><input name="pass" id="pass" type="password" /></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                        
                        <tr>
						<td id="mensaje" style="color:#F00; font-size:9px;" colspan="2"> 
                        		*Contraseña minimo 8 caracteres maximo 15
                        </td>
                   	    </tr>
                        
                         <tr>
                          <td height="27">Confirme contraseña:</td>
                          <td><input name="conf_password" id="conf_password" type="password"/></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                        </tr>
                      </table>
                      <p>
                      <fieldset>
                            <input class="submit-green" style="width:100px; height:30px; float:left; margin-left:12px;"  name="enviar" id="enviar" type='button'  value="Aceptar" onclick="return val_add()" />
                            </fieldset>
                      
                      </p>
                  
      </div> 
                <!-- End .module-body --><!-- End .container_12 -->
	</form>
    </div>
    </div>
    <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
        <?php include_once("z_script/footer.php") ?>   
        
</body>
</html>