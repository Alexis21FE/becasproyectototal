<?php 
session_start();

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}

$lib_rel_path = (count(explode("/", $_SERVER['SCRIPT_NAME']))-2 > 0 ? str_repeat("../", count(explode("/", $_SERVER['SCRIPT_NAME']))-2) : "");

include_once("../z_script/db_class.php");
mysql_select_db($bd_becas,$link);

include_once("../z_script/PHPExcel/Classes/PHPExcel.php");

$consulta="SELECT * FROM inf_familia ORDER BY id_fam ASC";
$result=mysql_query($consulta);
$i = 4;
$band=1;

$objPHPExcel = new PHPExcel();

$objPHPExcel->
	getProperties()
		->setCreator("Becas Administrator")
		->setLastModifiedBy("Becas Administrator")
		->setTitle("Informacion Madres de Familia")
		->setSubject("Usuarios")
		->setDescription("Documento generado por Colmenares");

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B1', 'Informacion de las Madres de Familia')
			->setCellValue('A3', 'ID Familia')
			->setCellValue('B3', 'Nombre')
			->setCellValue('C3', 'Ap Paterno')
			->setCellValue('D3', 'Ap Materno')
			->setCellValue('E3', 'Fecha Nacimiento')
			->setCellValue('F3', 'Edad')
			->setCellValue('G3', 'Domicilio')
			->setCellValue('H3', 'Ciudad o Municipio')
			->setCellValue('I3', 'Colonia')
			->setCellValue('J3', 'C.P.')
			->setCellValue('K3', 'Teléfono')
			->setCellValue('L3', 'Celular')
			->setCellValue('M3', 'Email')
			->setCellValue('N3', 'Ocupación')
			->setCellValue('O3', 'Nombre de la empresa')
			->setCellValue('P3', 'Giro de la empresa')
			->setCellValue('Q3', 'Puesto que desempeña')
			->setCellValue('R3', 'Antigüedad en la empresa (años)')
			->setCellValue('S3', '¿Es dueño o socio de la empresa?')
			->setCellValue('T3', '¿En qué porcentaje?');
			
			
while ($row = mysql_fetch_array($result)){ 
	$id_fam 	= $row["id_fam"];
	$mt_name 	= $row["mt_name"]; 
	$mt_ap 		= $row["mt_ap"];
	$mt_mat 	= $row["mt_mat"];
	$mt_date 	= $row["mt_date"];
	$mt_age 	= $row["mt_age"];
	$mt_dom		= $row["ft_dom"];
	$mt_cd		= $row["ft_cd"];
	$mt_col		= $row["ft_col"];
	$mt_cp		= $row["ft_cp"];
	$mt_tel		= $row["ft_tel"];
	$mt_cel		= $row["mt_cel"];
	$mt_email	= $row["mt_email"];
	$mt_ocup	= $row["mt_ocup"];
	$mt_emp		= $row["mt_emp"];
	$mt_giro	= $row["mt_giro"];
	$mt_pst		= $row["mt_pst"];
	$mt_ant		= $row["mt_ant"];
	$mt_empds	= $row["mt_empds"];
	$mt_porcen	= $row["mt_porcen"];
						
				

	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".$i."", $id_fam)
			->setCellValue("B".$i."", utf8_encode($mt_name))
            ->setCellValue("C".$i."", utf8_encode($mt_ap))
			->setCellValue("D".$i."", utf8_encode($mt_mat))
			->setCellValue("E".$i."", $mt_date)
			->setCellValue("F".$i."", $mt_age)
			->setCellValue("G".$i."", utf8_encode($mt_dom))
			->setCellValue("H".$i."", utf8_encode($mt_col))
			->setCellValue("I".$i."", $mt_cd)
			->setCellValue("J".$i."", $mt_cp)
			->setCellValue("K".$i."", $mt_tel)
			->setCellValue("L".$i."", $mt_cel)
			->setCellValue("M".$i."", $mt_email)
			->setCellValue("N".$i."", utf8_encode($mt_ocup))
			->setCellValue("O".$i."", utf8_encode($mt_emp))
			->setCellValue("P".$i."", utf8_encode($mt_giro))
			->setCellValue("Q".$i."", utf8_encode($mt_pst))
			->setCellValue("R".$i."", $mt_ant)
			->setCellValue("S".$i."", $mt_empds)
			->setCellValue("T".$i."", $mt_porcen);
						
	$i++;


}

for ($col = 'A'; $col != 'U'; $col++) {
	$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->setTitle('Usuarios Madres Administrador');
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="madre_info.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>