<?php
/**
 * PHPMailer language file.
 * Versi&oacute;n en espa�ol
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"]      = 'Debe proveer al menos una ' .
                                          'direcci&oacute;n de email como destinatario.';
$PHPMAILER_LANG["mailer_not_supported"] = ' mailer no est&aacute; soportado.';
$PHPMAILER_LANG["execute"]              = 'No puedo ejecutar: ';
$PHPMAILER_LANG["instantiate"]          = 'No pude crear una instancia de la funci&oacute;n Mail.';
$PHPMAILER_LANG["authenticate"]         = 'Error SMTP: No se pudo autentificar.';
$PHPMAILER_LANG["from_failed"]          = 'La(s) siguiente(s) direcciones de remitente fallaron: ';
$PHPMAILER_LANG["recipients_failed"]    = 'Error SMTP: Los siguientes ' .
                                          'destinatarios fallaron: ';
$PHPMAILER_LANG["data_not_accepted"]    = 'Error SMTP: Datos no aceptados.';
$PHPMAILER_LANG["connect_host"]         = 'Error SMTP: No puedo conectar al servidor SMTP.';
$PHPMAILER_LANG["file_access"]          = 'No puedo acceder al archivo: ';
$PHPMAILER_LANG["file_open"]            = 'Error de Archivo: No puede abrir el archivo: ';
$PHPMAILER_LANG["encoding"]             = 'Codificaci&oacute;n desconocida: ';
$PHPMAILER_LANG["signing"]              = 'Error al firmar: ';

?>
