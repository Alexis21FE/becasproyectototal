<?php 
session_start();

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}

$lib_rel_path = (count(explode("/", $_SERVER['SCRIPT_NAME']))-2 > 0 ? str_repeat("../", count(explode("/", $_SERVER['SCRIPT_NAME']))-2) : "");

include_once("../z_script/db_class.php");
include_once("../z_script/header.php");

include_once("../z_script/PHPExcel/Classes/PHPExcel.php");

$consulta="SELECT * FROM inf_familia ORDER BY id_fam ASC";
$result=$pdo->query($consulta);
$i = 4;
$band=1;

$objPHPExcel = new PHPExcel();

$objPHPExcel->
	getProperties()
		->setCreator("Becas Administrator")
		->setLastModifiedBy("Becas Administrator")
		->setTitle("Informacion Padres de Familia")
		->setSubject("Usuarios")
		->setDescription("Documento generado por Colmenares");

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B1', 'Informacion de lo Padres de Familia')
			->setCellValue('A3', 'ID Familia')
			->setCellValue('B3', 'Domicilio')
			->setCellValue('C3', 'Ciudad o Municipio')
			->setCellValue('D3', 'Colonia')
			->setCellValue('E3', 'C.P.')
			->setCellValue('F3', 'Teléfono')
			
			->setCellValue('G3', 'Nombre Padre')
			->setCellValue('H3', 'Ap Paterno')
			->setCellValue('I3', 'Ap Materno')
			->setCellValue('J3', 'Fecha Nacimiento')
			->setCellValue('K3', 'Edad')			
			->setCellValue('L3', 'Celular')
			->setCellValue('M3', 'Email')
			->setCellValue('N3', 'Ocupación')
			->setCellValue('O3', 'Nombre de la empresa')
			->setCellValue('P3', 'Giro de la empresa')
			->setCellValue('Q3', 'Puesto que desempeña')
			->setCellValue('R3', 'Antigüedad en la empresa (años)')
			->setCellValue('S3', '¿Es dueño o socio de la empresa?')
			->setCellValue('T3', '¿En qué porcentaje?')
			
			->setCellValue('U3', 'Nombre')
			->setCellValue('V3', 'Ap Paterno')
			->setCellValue('W3', 'Ap Materno')
			->setCellValue('X3', 'Fecha Nacimiento')
			->setCellValue('Y3', 'Edad')
			->setCellValue('Z3', 'Celular')
			->setCellValue('AA3', 'Email')
			->setCellValue('AB3', 'Ocupación')
			->setCellValue('AC3', 'Nombre de la empresa')
			->setCellValue('AD3', 'Giro de la empresa')
			->setCellValue('AE3', 'Puesto que desempeña')
			->setCellValue('AF3', 'Antigüedad en la empresa (años)')
			->setCellValue('AG3', '¿Es dueño o socio de la empresa?')
			->setCellValue('AH3', '¿En qué porcentaje?');
			

?>
       <table id="myTable" class="tablesorter table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Familia</th>
                                    <th>Nombre Familia</th>
                                    <th >Nombre Padre</th>
                                    <th >Celular Padre</th>
                                    <th >Correo electrónico Padre</th>
                                    <th >Nombre Madre</th>
                                    <th >Celular Madre</th>
                                    <th >Correo electrónico Madre</th>
                                    <th >Teléfono</th>
                                    <th >Direccion</th>
                                    <th>Opciones</th>
                                </tr>
                        </thead>
                            <?php
								
							
								foreach ($result as $row){
								$x_id_fam=$row["id_fam"];	
								$x_ft_name=$row["ft_name"];
								$x_ft_ap=$row["ft_ap"];
								$x_ft_mat=$row["ft_mat"];
								$x_ft_tel=$row["ft_tel"];
								$x_ft_cel=$row["ft_cel"];
								$x_ft_email=$row["ft_email"];
																	
								$x_mt_name=$row["mt_name"];
								$x_mt_ap=$row["mt_ap"];
								$x_mt_mat=$row["mt_mat"];
								$x_mt_cel=$row["mt_cel"];
								$x_mt_email=$row["mt_email"];
								$x_ft_dom=$row['ft_dom'];
								$x_ft_cp=$row['ft_cp'];
								$x_ft_colonia=$row['ft_col'];
								$x_ft_municipio=$row['ft_cd'];
								$usuario=$row["usuario"];
								
								if($x_ft_cel == NULL || $x_ft_cel == 0){ $x_ft_cel="";}
								if($x_ft_tel == NULL || $x_ft_tel == 0){ $x_ft_tel="";}
								if($x_ft_email == NULL){ $x_ft_email="";}
								
								if($x_mt_cel == NULL || $x_mt_cel == 0){ $x_mt_cel="";}
								if($x_mt_email == NULL){ $x_mt_email="";}
								
								$name_del_ft=$x_ft_name." ".$x_ft_ap." ".$x_ft_mat;
								$name_del_mt=$x_mt_name." ".$x_mt_ap." ".$x_mt_mat;
								
								?>
                            <tbody>
                                <tr>
             <td class="align-center"><?php echo $band_id;?></td>
             <td class="align-center"><?php echo $usuario;?></td>
             <td class="align-center"><?php echo $x_ft_ap." ".$x_mt_ap;?></td>
             <td><?php echo $x_ft_name." ".$x_ft_ap." ".$x_ft_mat;?></td>             
             <td><?php echo $x_ft_cel;?></td>
             <td><?php echo $x_ft_email;?></td>
			 
			 <td><?php echo $x_mt_name." ".$x_mt_ap." ".$x_mt_mat;?></td>             
             <td><?php echo $x_mt_cel;?></td>
             <td><?php echo $x_mt_email;?></td>
			 
			 <td><?php echo $x_ft_tel;?></td>
			 <td><?php echo $x_ft_dom;?></td>
			 <td><?php echo $x_ft_cp;?></td>
			 <td><?php echo $x_ft_colonia;?></td>
			 <td><?php echo $x_ft_municipio;?></td>
             <td>
                                    
<a href="ft_perfil.php?id=
<?php echo $x_id_fam;?>&usuario=
<?php echo $usuario;?>">
<i class="far fa-user"></i></a>
<a href="ft_update.php?id=
<?php echo $x_id_fam;?>&usuario=
<?php echo $usuario;?>">
    <i class="far fa-edit"></i></a>

<a onclick="opcion(<?php echo $x_id_fam." , '".$name_del_ft."', '".$usuario."'";?>);" ><i class="fas fa-user-minus"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                          <?php $band_id++; }  ?>
                        </table>
<script type="text/javascript">
    $(document).ready(function(){
         $("#myTable").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "Reporte.xls" //do not include extension

  });

 var  url="../report.php";
       setTimeout('closemyself()',1000);
    });
      function closemyself() {
 window.opener=self;
 window.close();
 //self.close();
}
</script>