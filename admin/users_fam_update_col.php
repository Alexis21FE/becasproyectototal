<?php 
session_start();
$_SESSION["var_ban"]=3;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Colegio") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$usuario = $_GET["usuario"];

if($usuario=="")
{
	$usuario = $_POST["usuario"];
}
$fam_alum='';
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
extract($_POST, EXTR_PREFIX_ALL, "x");
$fecha_year = date("Y");
$fech_month = date("m");
$fam_id = $_GET["id"];
$al_=0;
include("z_script/db_class.php");	
mysql_select_db($bd_becas, $link);

$sqlA="SELECT count(alum_mat) as al FROM inf_alum WHERE usuario='$usuario'"; 
 $resultA=mysql_query($sqlA);

while ($rowA = mysql_fetch_array($resultA)){ 
		$alumActivo  = $rowA["al"];
		}
if($alumActivo<=0)
{
	echo" <script language='javascript'>alert('No hay alumnos activos para esta familia');</script>";
}
				
 $sql="SELECT * FROM user_fam WHERE fam_user='$usuario'"; 
 $result=mysql_query($sql);

while ($row = mysql_fetch_array($result)){ 
		$x_fam_user  = $row["fam_user"];
		$x_fam_pass	 = $row["fam_pass"];
		$x_fam_ade 	 = $row["fam_ade"];
		$x_fam_term	 = $row["fam_term"];
		$x_acep_term	 = $row["acepta_term"];
		$x_pago_soc	 = $row["pago_soc"];
		$x_cod_verif = $row["cod_verif"];
		$x_fech_inc = $row["fech_inc"];
		$x_fech_fin = $row["fech_fin"];
		$x_obser_fam = $row["obser_fam"];
		$acceso = $row["acc_directo"];
		
}
				
$fec_inc = explode ("-", $x_fech_inc); 
$x_dia_inc	= $fec_inc[2];
$x_mes_inc	= $fec_inc[1];
$x_year_inc	= $fec_inc[0];

$fec_fin = explode ("-", $x_fech_fin); 
$x_dia_fin	= $fec_fin[2];
$x_mes_fin	= $fec_fin[1];
$x_year_fin	= $fec_fin[0];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
        <style type="text/css">
		div.module table td {
		background-color: #ffffff;
		padding: 5px;
		border-right: 0px solid #d9d9d9;
		}

		div.module table {
			width: 0%;
			margin: 0 0 10px 0;
			border-left: 0px solid #d9d9d9;
			border-bottom: 0px solid #d9d9d9;
			}
		</style>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
            
            
            
            <div class="grid_12">
                <div class="module">
                	<h2><span>Modificar Usuario Familia: <?php echo $fam_id; ?></span></h2>
                    
                    <div class="module-table-body" >
                    	<div style="margin-left: 25px;margin-top: 10px;">
                        <form name="upd_user" action="actions/users_fam_mod_col.php?id=<?php echo $fam_id; ?>&usuario=<?php echo $usuario; ?>" method="post" enctype="multipart/form-data">
                    	  <table width="893" border="0" style="border-left: 0px solid #d9d9d9; border-bottom: 0px solid #d9d9d9;">
                    	    <tr>
                            <td width="134">Usuario Familia:</td>
                            <td width="306"><input type="text" name="fam_user" id="fam_user" value="<?php echo $x_fam_user; ?>" /></td>
                            <td width="439"></td>
          					</tr>
                          <tr>
                            <td>Contraseña:</td>
                            <td><input type="text" name="fam_pass" id="fam_pass" value="<?php echo $x_fam_pass;?>" /></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Adeudo:</td>
                            <td><select id="fam_ade" name="fam_ade">
                              <option value="" <?php if($x_fam_ade == ""){echo "selected='selected'";} ?>>--</option>
							  <option value="Si" <?php if($x_fam_ade == "Si"){echo "selected='selected'";} ?>>Si</option>
                              <option value="No" <?php if($x_fam_ade == "No"){echo "selected='selected'";} ?>>No</option>
                            </select></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Acepto Términos:</td>
                            <td><select id="fam_term" name="fam_term">
                              <option value="Si" <?php if($x_fam_term == "Si"){echo "selected='selected'";} ?>>Si</option>
                              <option value="No" <?php if($x_fam_term == "No"){echo "selected='selected'";} ?>>No</option>
                            </select></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Pago Estudio Soc-Eco:</td>
                            <td><select id="pago_soc" name="pago_soc">
                              <option value="Si" <?php if($x_pago_soc == "Si"){echo "selected='selected'";} ?>>Si</option>
                              <option value="No" <?php if($x_pago_soc == "No"){echo "selected='selected'";} ?>>No</option>
                            </select></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Código Verificación:</td>
                            <td><input type="text" name="cod_verif" id="cod_verif" value="<?php echo $x_cod_verif;?>" /></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Fecha Inicio:</td>
                            <td><span style="float:left; width:200px;">
                              <select name="dia_inc" id="dia_inc">
                                <option value="01" <?php if($x_dia_inc == "01"){echo "selected='selected'";} ?>>1</option>
                                <option value="02" <?php if($x_dia_inc == "02"){echo "selected='selected'";} ?>>2</option>
                                <option value="03" <?php if($x_dia_inc == "03"){echo "selected='selected'";} ?>>3</option>
                                <option value="04" <?php if($x_dia_inc == "04"){echo "selected='selected'";} ?>>4</option>
                                <option value="05" <?php if($x_dia_inc == "05"){echo "selected='selected'";} ?>>5</option>
                                <option value="06" <?php if($x_dia_inc == "06"){echo "selected='selected'";} ?>>6</option>
                                <option value="07" <?php if($x_dia_inc == "07"){echo "selected='selected'";} ?>>7</option>
                                <option value="08" <?php if($x_dia_inc == "08"){echo "selected='selected'";} ?>>8</option>
                                <option value="09" <?php if($x_dia_inc == "08"){echo "selected='selected'";} ?>>9</option>
                                <option value="10" <?php if($x_dia_inc == "10"){echo "selected='selected'";} ?>>10</option>
                                <option value="11" <?php if($x_dia_inc == "11"){echo "selected='selected'";} ?>>11</option>
                                <option value="12" <?php if($x_dia_inc == "12"){echo "selected='selected'";} ?>>12</option>
                                <option value="13" <?php if($x_dia_inc == "13"){echo "selected='selected'";} ?>>13</option>
                                <option value="14" <?php if($x_dia_inc == "14"){echo "selected='selected'";} ?>>14</option>
                                <option value="15" <?php if($x_dia_inc == "15"){echo "selected='selected'";} ?>>15</option>
                                <option value="16" <?php if($x_dia_inc == "16"){echo "selected='selected'";} ?>>16</option>
                                <option value="17" <?php if($x_dia_inc == "17"){echo "selected='selected'";} ?>>17</option>
                                <option value="18" <?php if($x_dia_inc == "18"){echo "selected='selected'";} ?>>18</option>
                                <option value="19" <?php if($x_dia_inc == "19"){echo "selected='selected'";} ?>>19</option>
                                <option value="20" <?php if($x_dia_inc == "20"){echo "selected='selected'";} ?>>20</option>
                                <option value="21" <?php if($x_dia_inc == "21"){echo "selected='selected'";} ?>>21</option>
                                <option value="22" <?php if($x_dia_inc == "22"){echo "selected='selected'";} ?>>22</option>
                                <option value="23" <?php if($x_dia_inc == "23"){echo "selected='selected'";} ?>>23</option>
                                <option value="24" <?php if($x_dia_inc == "24"){echo "selected='selected'";} ?>>24</option>
                                <option value="25" <?php if($x_dia_inc == "25"){echo "selected='selected'";} ?>>25</option>
                                <option value="26" <?php if($x_dia_inc == "26"){echo "selected='selected'";} ?>>26</option>
                                <option value="27" <?php if($x_dia_inc == "27"){echo "selected='selected'";} ?>>27</option>
                                <option value="28" <?php if($x_dia_inc == "28"){echo "selected='selected'";} ?>>28</option>
                                <option value="29" <?php if($x_dia_inc == "29"){echo "selected='selected'";} ?>>29</option>
                                <option value="30" <?php if($x_dia_inc == "30"){echo "selected='selected'";} ?>>30</option>
                                <option value="31" <?php if($x_dia_inc == "31"){echo "selected='selected'";} ?>>31</option>
                              </select>
                              <select name="mes_inc" id="mes_inc">
                                <option value="01" <?php if($x_mes_inc == "01"){echo "selected='selected'";} ?>>Ene</option>
                                <option value="02" <?php if($x_mes_inc == "02"){echo "selected='selected'";} ?>>Feb</option>
                                <option value="03" <?php if($x_mes_inc == "03"){echo "selected='selected'";} ?>>Mar</option>
                                <option value="04" <?php if($x_mes_inc == "04"){echo "selected='selected'";} ?>>Apr</option>
                                <option value="05" <?php if($x_mes_inc == "05"){echo "selected='selected'";} ?>>May</option>
                                <option value="06" <?php if($x_mes_inc == "06"){echo "selected='selected'";} ?>>Jun</option>
                                <option value="07" <?php if($x_mes_inc == "07"){echo "selected='selected'";} ?>>Jul</option>
                                <option value="08" <?php if($x_mes_inc == "08"){echo "selected='selected'";} ?>>Aug</option>
                                <option value="09" <?php if($x_mes_inc == "09"){echo "selected='selected'";} ?>>Sep</option>
                                <option value="10" <?php if($x_mes_inc == "10"){echo "selected='selected'";} ?>>Oct</option>
                                <option value="11" <?php if($x_mes_inc == "11"){echo "selected='selected'";} ?>>Nov</option>
                                <option value="12" <?php if($x_mes_inc == "12"){echo "selected='selected'";} ?>>Dec</option>
                              </select>
                              <select name="year_inc" id="year_inc">
                                <option value="" selected="selected">Año</option>
                                <?php
											if($fech_month == 01){	 
											echo "<option value='".($fecha_year-1)."'>".($fecha_year-1)."</option>";
								
											}
											
											if($x_year_inc == $fecha_year ){
												echo "<option value='".$fecha_year."' selected='selected'>".$fecha_year."</option>";
											}else{
												echo "<option value='".$fecha_year."'>".$fecha_year."</option>";
											}
											
											if($x_year_inc == ($fecha_year+1) ){
												echo "<option value='".($fecha_year+1)."' selected='selected'>".($fecha_year+1)."</option>";
											}else{
												echo "<option value='".($fecha_year+1)."'>".($fecha_year+1)."</option>";
											}
																		
										
									?>
                              </select>
                            </span></td>
                            <td></td>
                            </tr>
                           <tr>     
                            <td>Fecha Final:</td>
                            <td><span style="float:left; width:200px;">
                            <select name="dia_fin" id="dia_fin">
                                <option value="01" <?php if($x_dia_fin == "01"){echo "selected='selected'";} ?>>1</option>
                                <option value="02" <?php if($x_dia_fin == "02"){echo "selected='selected'";} ?>>2</option>
                                <option value="03" <?php if($x_dia_fin == "03"){echo "selected='selected'";} ?>>3</option>
                                <option value="04" <?php if($x_dia_fin == "04"){echo "selected='selected'";} ?>>4</option>
                                <option value="05" <?php if($x_dia_fin == "05"){echo "selected='selected'";} ?>>5</option>
                                <option value="06" <?php if($x_dia_fin == "06"){echo "selected='selected'";} ?>>6</option>
                                <option value="07" <?php if($x_dia_fin == "07"){echo "selected='selected'";} ?>>7</option>
                                <option value="08" <?php if($x_dia_fin == "08"){echo "selected='selected'";} ?>>8</option>
                                <option value="09" <?php if($x_dia_fin == "08"){echo "selected='selected'";} ?>>9</option>
                                <option value="10" <?php if($x_dia_fin == "10"){echo "selected='selected'";} ?>>10</option>
                                <option value="11" <?php if($x_dia_fin == "11"){echo "selected='selected'";} ?>>11</option>
                                <option value="12" <?php if($x_dia_fin == "12"){echo "selected='selected'";} ?>>12</option>
                                <option value="13" <?php if($x_dia_fin == "13"){echo "selected='selected'";} ?>>13</option>
                                <option value="14" <?php if($x_dia_fin == "14"){echo "selected='selected'";} ?>>14</option>
                                <option value="15" <?php if($x_dia_fin == "15"){echo "selected='selected'";} ?>>15</option>
                                <option value="16" <?php if($x_dia_fin == "16"){echo "selected='selected'";} ?>>16</option>
                                <option value="17" <?php if($x_dia_fin == "17"){echo "selected='selected'";} ?>>17</option>
                                <option value="18" <?php if($x_dia_fin == "18"){echo "selected='selected'";} ?>>18</option>
                                <option value="19" <?php if($x_dia_fin == "19"){echo "selected='selected'";} ?>>19</option>
                                <option value="20" <?php if($x_dia_fin == "20"){echo "selected='selected'";} ?>>20</option>
                                <option value="21" <?php if($x_dia_fin == "21"){echo "selected='selected'";} ?>>21</option>
                                <option value="22" <?php if($x_dia_fin == "22"){echo "selected='selected'";} ?>>22</option>
                                <option value="23" <?php if($x_dia_fin == "23"){echo "selected='selected'";} ?>>23</option>
                                <option value="24" <?php if($x_dia_fin == "24"){echo "selected='selected'";} ?>>24</option>
                                <option value="25" <?php if($x_dia_fin == "25"){echo "selected='selected'";} ?>>25</option>
                                <option value="26" <?php if($x_dia_fin == "26"){echo "selected='selected'";} ?>>26</option>
                                <option value="27" <?php if($x_dia_fin == "27"){echo "selected='selected'";} ?>>27</option>
                                <option value="28" <?php if($x_dia_fin == "28"){echo "selected='selected'";} ?>>28</option>
                                <option value="29" <?php if($x_dia_fin == "29"){echo "selected='selected'";} ?>>29</option>
                                <option value="30" <?php if($x_dia_fin == "30"){echo "selected='selected'";} ?>>30</option>
                                <option value="31" <?php if($x_dia_fin == "31"){echo "selected='selected'";} ?>>31</option>
                              </select>
                              <select name="mes_fin" id="mes_fin">
                                <option value="01" <?php if($x_mes_fin == "01"){echo "selected='selected'";} ?>>Ene</option>
                                <option value="02" <?php if($x_mes_fin == "02"){echo "selected='selected'";} ?>>Feb</option>
                                <option value="03" <?php if($x_mes_fin == "03"){echo "selected='selected'";} ?>>Mar</option>
                                <option value="04" <?php if($x_mes_fin == "04"){echo "selected='selected'";} ?>>Apr</option>
                                <option value="05" <?php if($x_mes_fin == "05"){echo "selected='selected'";} ?>>May</option>
                                <option value="06" <?php if($x_mes_fin == "06"){echo "selected='selected'";} ?>>Jun</option>
                                <option value="07" <?php if($x_mes_fin == "07"){echo "selected='selected'";} ?>>Jul</option>
                                <option value="08" <?php if($x_mes_fin == "08"){echo "selected='selected'";} ?>>Aug</option>
                                <option value="09" <?php if($x_mes_fin == "09"){echo "selected='selected'";} ?>>Sep</option>
                                <option value="10" <?php if($x_mes_fin == "10"){echo "selected='selected'";} ?>>Oct</option>
                                <option value="11" <?php if($x_mes_fin == "11"){echo "selected='selected'";} ?>>Nov</option>
                                <option value="12" <?php if($x_mes_fin == "12"){echo "selected='selected'";} ?>>Dec</option>
                              </select>
                              <select name="year_fin" id="year_fin">
                                <option value="" selected="selected">Año</option>
                                <?php
										
										if($fech_month == 01){	 
											echo "<option value='".($fecha_year-1)."'>".($fecha_year-1)."</option>";
										}	 
											
										if($x_year_fin == $fecha_year ){
												echo "<option value='".$fecha_year."' selected='selected'>".$fecha_year."</option>";
											}else{
												echo "<option value='".$fecha_year."'>".$fecha_year."</option>";
											}
											
											if($x_year_fin == ($fecha_year+1) ){
												echo "<option value='".($fecha_year+1)."' selected='selected'>".($fecha_year+1)."</option>";
											}else{
												echo "<option value='".($fecha_year+1)."'>".($fecha_year+1)."</option>";
											}						
										
									?>
                              </select>
                            </span></td>
                          <td></td>
                          </tr>
                          <tr>
                            <td style="margin-top: 30px;position: absolute;">Observaciones:</td>
                            <td><textarea name="obser_fam" id="obser_fam" cols="45" rows="5"><?php echo $x_obser_fam;?></textarea></td>
                            <td></td>
                          </tr>
						  <?
						  
						  if($acceso=='S')
						  	$check="checked";
						  ?>
						  <tr>
                            <td>Permitir Acceso:</td>
                            <td><label>
							
							<select name="acceso" id="acceso">
                                <option value="" selected="selected"> Selecciona </option>
                                <?php
										
										if($acceso == 'S'){	 
											echo "<option value=S selected='selected'>Si</option>";
											}else{
												echo "<option value='S'>Si</option>";
											}
									?>
                               </select>
							
                            </label></td>
                            <td></td>
                          </tr>
                          
                    </table>
                       
                        
                        <h4>
                        <input class="submit-green" style="width:90px; height:30px; margin-top:20px"  name="enviar" id="enviar" type='submit'  value="Modificar" />
                        </h4>
                         </form>
                    	</div>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->

				
				
				
				
                
			</div> 
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>	
</html>