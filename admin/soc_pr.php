<?php
session_start();
if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
		
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
extract($_POST, EXTR_PREFIX_ALL, "x");
$_SESSION["var_ban"]=6;

$ban_green = (isset ($ban_green) ? $ban_green: "");
$ban_red = (isset ($ban_red) ? $ban_red: "");

$minimo=$_POST['minimo'];

include("z_script/db_class.php");
mysql_select_db($bd_becas, $link);
$sql="SELECT * FROM soc_price"; 
$result=mysql_query($sql);

while ($row = mysql_fetch_array($result)){
	
	$sist_price =$row["price"];
	$adeudo =$row["ade_minimo"];
	
}

if(isset($x_enviar)){
	
	if($x_price_bec == NULL ){

		$ban_red="incorrect";	
	}else{

			$sql="UPDATE soc_price SET price='".$x_price_bec."', ade_minimo='".$minimo."'"; 
			$result=mysql_query($sql);
			
			
			$sql="SELECT * FROM soc_price"; 
			$result=mysql_query($sql);
			
			while ($row = mysql_fetch_array($result)){
				
				$sist_price =$row["price"];
				
			}
			
			$ban_green="aproved";
		}	
		
	
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <script type="text/javascript" src="js/vald_info.js"></script>
		<?php
            include_once("z_script/header.php");
        ?>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">
            
            <div style="clear:both;"></div>
            
            
            
            <div class="grid_12">
            	<?php
					if($ban_green=="aproved"){
						?>
						<span class="notification n-success">Precio Estudio Socio-Económico con exito.</span>
                <?php        		
					}
					 if($ban_red=="incorrect"){
				?>
                	   <span class="notification n-error">Ingrese correctamente el precio.</span>
                <?php		
					}
					
				?>	
				
                <div class="bottom-spacing"></div>
                
                
                <!-- Example table --><!-- End .module --></div> <!-- End .grid_12 -->
                
            <!-- Categories list --><!-- End .grid_6 -->
            
            <!-- To-do list --><!-- End .grid_6 -->
          <div style="clear:both;"></div>
            
            <!-- Form elements -->    
            <div class="grid_12">
            
           	  <div class="module" style="width:350px">
                     <h2><span>Costo actual del Estudio-Socioeconómico en el sistema.</span></h2>
                        
                     <div class="module-body">
                        
                        <h5 style="margin-left:20px;">Costo: <?php echo $sist_price;?></h5>
						
						<h5 style="margin-left:20px;">Adeudo máximo permitido: <?php echo $adeudo;?></h5>
                   </div> <!-- End .module-body -->
				   

                </div>
            
                <div class="module">
                     <h2><span>Modificar Costo del Estudio Socio-Económico</span></h2>
                        
                     <div class="module-body">
                        <form name="form_price" action="soc_pr.php" enctype="multipart/form-data" method="post">   
                            Nota: * Siempre incluir el costo con 2 puntos decimales: XXX.00
                            <div style="float:left; width:300px;">
                                <label>Ingresa el nuevo costo:</label>
                                <input name="price_bec" id="price_bec" type="text" />
                          </div>
						  
						  <div style="float:left; width:300px;">
                                <label>Ingresa adeudo máximo permitido:</label>
                                <input name="minimo" id="minimo" type="text" />
                          </div>
						  
						  
						  
                                <div style="float:left; margin-top:26px;">
                                
                                <div id="msg_red_<?php echo $x_cont_msg=0; ?>" class="notification-input ni-error" style="display:none;"></div>
                                </div>

                          <div class="bottom-spacing"><br />
							<br />
                          </div>
                          <div style="float:left; width:200px;">
                            <fieldset>
                            <input class="submit-green"  name="enviar" id="enviar" type="submit" value="Modificar" onclick="return fech_selec()" />
                            </fieldset> </div>
                              
                             
                            
                            
                        </form>
                     </div> <!-- End .module-body -->

                </div>  <!-- End .module -->
        		<div style="clear:both;"></div>
            </div> <!-- End .grid_12 -->
                
            <!-- Settings-->
            <div class="grid_6">
                 <!-- End .module -->
            </div> <!-- End .grid_6 -->
                
            <!-- Password --><!-- End .grid_6 -->
          <div style="clear:both;"></div><!-- End .grid_3 --><!-- End .grid_3 --><!-- End .grid_6 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
        <?php include_once("z_script/footer.php") ?>   

	</body>
</html>