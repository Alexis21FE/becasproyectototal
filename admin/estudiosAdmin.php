<?php 
session_start();

	include_once("z_script/db_class.php");
?>
<!DOCTYPE html>

<html>
<head>
	  <?php

            include_once("z_script/header.php");

        ?>
</head>
<body>
   <?php

    include_once("z_script/menu.php");

    ?>

                    <div class="col">
                        <center><h3>EMPRESAS DE ESTUDIOS SOCIO-ECONÓMICOS</h3></center><hr>
                        </div>
                    <div class="row" style="margin:auto;width: 90%;">
                        <h4>AGREGAR EMPRESA</h4>
                        </div>
                        <div class="row" style="margin:auto;width: 90%;">
                        	<div class="col">
                        		<label>Nombre de la empresa</label>
                        		<input type="text" name="nombreEmpresa" id="empresaNombre" placeholder="Nombre de empresa" class="form-control" />
                        	</div>
                        		<div class="col">
                        		<label>Correo</label>
                        		<input type="text" name="empresaMail" id="empresaMail" placeholder="empresa@example.com" class="form-control" />
                        	</div>
                        		<div class="col">
                        		<label>Contraseña</label>
                        		<input type="password" name="empresaPass" id="empresaPass" placeholder="Contraseña"  class="form-control" />
                        	</div>
                        	<div class="col">
                        		<button type="button" name="btnAddEmpresa" id="btnAddEmpre" class="btn btn-success" style="    margin-top: 11%;">Agregar</button>
                        	</div>
                        </div>
                            <br/>
                            <br/>
                            
                        <div class="row" style="margin:auto;width: 90%;">
                        <h4>LISTA DE EMPRESAS</h4>
                        </div>
                            
                        <div class="row" style="margin:auto;width: 90%;">
                        	<div class="col">
                                    <label>Nombre de la empresa</label>
                        	</div>
                        	<div class="col">
                        		<label>Correo</label>
                        	</div>
                        	<div class="col">
                        		<label>Contraseña</label>
                        	</div>                        	
                        </div>
                        
                        	<?php
                        	$sqlEmpresas="Select * from empresas_estudios ";
					   		$empresas=$pdo->query($sqlEmpresas);
                        		foreach($empresas as $empresa)
                        		{
                  	?>
    
                           
    				<form method="POST" name="form2" id="<?php echo "formulario".$empresa['idEmpresa'];?>"> 
            				<div id="<?php echo "Form".$empresa['idEmpresa'];?>" class="row" style="margin:auto;width: 90%;">
            			
                        		<div class="col" style="margin-top: 1%;">
                        		<input type="text" name="nombre" value="<?php echo $empresa['Nombre']; ?>"  readonly placeholder="Nombre de empresa" class="form-control" />
                        	</div>
                        		<div class="col" style="margin-top: 1%;">
                        		<input type="email" name="mail" value="<?php echo $empresa['Email']; ?>" readonly placeholder="empresa@example.com" class="form-control" />
                        	</div>
                        		<div class="col"style="margin-top: 1%;">
                        		<input type="password" name="pass" value="<?php echo $empresa['Password']; ?>" readonly  placeholder="Contraseña"  class="form-control" />
                        	</div>                                            
                        		<input type="hidden" name="function" value="saveEmpresa"  class="form-control" />
                                	<input type="hidden" name="id" value="<?php echo $empresa['idEmpresa']; ?>"  class="form-control" />
                        	
                        	<div class="col" style="margin-top: 1%;">
                        		<button type="button" name="btnDeletempresa" id="<?php echo "btnDeleteEmpresa".$empresa['idEmpresa'];?>" onClick="eliminar(<?php echo $empresa['idEmpresa'];?>)" class="btn btn-danger" > Eliminar
                        		</button>
                                        <button type="button" name="btnEditEmpresa" id="<?php echo "btnEditEmpresa".$empresa['idEmpresa'];?>" onClick="editar(<?php echo $empresa['idEmpresa'];?>)" class="btn btn-warning"  > Editar
                        		</button>
                        		<button type="button" name="btnSaveEmpresa" id="<?php echo "btnSaveEmpresa".$empresa['idEmpresa'];?>" onClick="guardar(<?php echo $empresa['idEmpresa'];?>)" class="btn btn-success" >Guardar
                        		</button>
                        	</div>

                        </div>
                      </form> 
                        <?php } ?>
                         <div style="clear:both;"></div>
          <?php include_once("z_script/footer.php"); ?>
</body>
<script  type="text/javascript">
	$("#btnAddEmpre").click(function(){
		var nombre=$("#empresaNombre").val();
		var email=$("#empresaMail").val();
		var pass=$("#empresaPass").val();
		if(nombre!=''){
			$.ajax({
				url:"actions/empresasActions.php",
				data:{
					"nombre":nombre,
					"mail":email,
					"pass":pass,
					"function":"addEmpresa"
				},
				datatype:"json",
				type:"post",
				success:function(response){
					if(response.status){
                                        alert(response.mensaje);
                                        
					}  
				}
			});
			location.reload();
		}else{
			alert("Ingrese un nombre");
                        
		}
                
	});
        
	function editar(id){
    $("#Form"+id).find('input').prop('readonly',false);
  }
  
   function eliminar(id){
    $.ajax({
            url:"actions/empresasActions.php",
            data:{
                    "id" : id,
                    "function":"deleteEmpresa"
            },
            datatype:"json",
            type:"post",
            success:function(response){
                    if(response.status){
                       alert(response.mensaje);
                    }
                   
            }
    });
  
     location.reload();
  }
  
 function guardar(id){
     var form=$("#Form"+id).find('input').serialize();
     console.log(form);
    $.ajax({
            url:"actions/empresasActions.php",
            data:form,
            datatype:"json",
            type:"post",
            success:function(response){
                    if(response.status){
                       alert(response.mensaje);
                    }
                   
            }
    });

    location.reload();
  }
  
</script>
</html>
