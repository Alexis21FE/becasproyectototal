<?php 
session_start();

	//include_once("z_script/db_class.php");
include ('../estudios/Funciones/conexion.php');
$_SESSION["colegio"];
$selectFamilias="Select DISTINCT ets.*,inf.* from estudios_familias ets inner join inf_familia inf on ets.idFam=inf.id_fam 
INNER JOIN inf_alum ia ON ets.idFam=ia.id_fam
WHERE ets.cierre=1 AND ia.alum_colg='".$_SESSION['colegio']."'";

$resFamilias=$pdo->query($selectFamilias);
?>
<!DOCTYPE html>

<html>
<head>
	  <?php

            include_once("z_script/header.php");

        ?>
</head>
<body>
   <?php

    include_once("z_script/menu.php");

    ?>

                       <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" >

            <thead class="thead-dark">
                <tr>
                    <th  class="th-sm">Nombre Familia</th>
                    <th  class="th-sm">Fecha Asignado</th> 
                    <th  class="th-sm">Fecha Realizado</th>
                    <th  class="th-sm">Encuesta</th>
                </tr>
            </thead>
             <tbody>
                             <?php foreach($resFamilias as $familia){
                    $fechaC=$familia['fechaCierre'];
                $fechaM=$familia['fechaMod'];
               
                   $fechaCierre = new DateTime($fechaC);
                   $fechaMod = new DateTime($fechaM);

              ?>
    <tr>
<th  class="th-sm"><?php echo $familia['ft_ap']." ".$familia['mt_ap']?></th>
              
                    <th  class="th-sm"><?php echo $fechaMod->format('Y-m-d')?></th> 
                    <th  class="th-sm"><?php echo  $fechaCierre->format('Y-m-d')?></th>
                    <th><a href="../estudios/Funciones/formulario.php?usuario=<?php echo $familia['usuario'];?>&idFam=<?php echo $familia['id_fam']?>"><i class="far fa-edit"></i></a></th>  
    </tr>
    <?php  } ?>
            </tbody>
           <tfoot>
<th  class="th-sm">Nombre Familia</th>
                    <th  class="th-sm">Fecha Asignado</th> 
                    <th  class="th-sm">Fecha Realizado</th>
                    <th  class="th-sm">Encuesta</th>
  </tfoot>
        </table>
                         <div style="clear:both;"></div>
          <?php include_once("z_script/footer.php"); ?>
</body>
<script  type="text/javascript">
	$("#btnAddEmpre").click(function(){
		var nombre=$("#empresaNombre").val();
		var email=$("#empresaMail").val();
		var pass=$("#empresaPass").val();
		if(nombre!=''){
			$.ajax({
				url:"actions/empresasActions.php",
				data:{
					"nombre":nombre,
					"mail":email,
					"pass":pass,
					"function":"addEmpresa"
				},
				datatype:"json",
				type:"post",
				success:function(response){
					if(response.status){
                                        alert(response.mensaje);
                                        
					}  
				}
			});
			location.reload();
		}else{
			alert("Ingrese un nombre");
                        
		}
                
	});
        
	function editar(id){
    $("#Form"+id).find('input').prop('readonly',false);
  }
  
   function eliminar(id){
    $.ajax({
            url:"actions/empresasActions.php",
            data:{
                    "id" : id,
                    "function":"deleteEmpresa"
            },
            datatype:"json",
            type:"post",
            success:function(response){
                    if(response.status){
                       alert(response.mensaje);
                    }
                   
            }
    });
  
     location.reload();
  }
  
 function guardar(id){
     var form=$("#Form"+id).find('input').serialize();
     console.log(form);
    $.ajax({
            url:"actions/empresasActions.php",
            data:form,
            datatype:"json",
            type:"post",
            success:function(response){
                    if(response.status){
                       alert(response.mensaje);
                    }
                   
            }
    });

    location.reload();
  }
  
</script>
</html>
